import json
cafe_data = open('cafe.json', 'r')
cafe = json.loads(cafe_data.read()) 

palces_data = open('places.json', 'r')
places = json.loads(palces_data.read()) 

def getPlaceId(searchString,cafeName, placeId):
    """ search cafe Name and place_Id by sub string

    Args:
        searchString (string): input string
        cafeName (list): list of all cafes name matching string
        placeId (list): list of all places id matching particular cafe name
    """
    for x in cafe:
        if searchString in x['name']:
            placeId.append(x['place_id'])
            cafeName.append({'name':x['name']})
    

def getPlaceDict(placeId):
    """getting Place Data by place_Id

    Args:
        placeId (list): list containing place_Id

    Returns:
        list: list of place Data
    """
    placeDict=[]
    for y in placeId:
        for x in places:
            if x['id'] == y:
                placeDict.append(x)
    return placeDict

def mergeCafePlace(placeData, cafeName, result):
    """ merge CafeName With PlaceData

    Args:
        placeData ([type]): [description]
        cafeName ([type]): [description]
        result ([type]): [description]
    """
    l = len(placeData)    
    for x in range(0,l):
        del placeData[x]['id']
        cafeNameElement=cafeName[x]
        placeDataElement=placeData[x]
        res = {**cafeNameElement, **placeDataElement}  
        result.append(res)
    

def findCaliforniaCafes(searchString):
    placeId=[]
    cafeName=[]
    result = []
    getPlaceId(searchString,cafeName, placeId)
    placeData=getPlaceDict(placeId)
    mergeCafePlace(placeData, cafeName, result)
    return result

print(findCaliforniaCafes('Avenue'))